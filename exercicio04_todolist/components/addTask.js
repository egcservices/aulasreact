import React, {Component} from 'react';
import {StyleSheet, View} from 'react-native';
import {Button, TextInput} from 'react-native-paper';

export default class AddTask extends Component {

    state = {
        text: ''
    };

    render() {
        return (
            <View style={styles.container}>
                <TextInput
                    value={this.state.text}
                    placeholder="Digite a task"
                    onChangeText={(text) => this.setState({text})}/>
                <Button
                    primary
                    raised
                    onPress={() => this.props.onTaskFilled(this.state.text)}>Add</Button>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        height: 160,
        backgroundColor: '#fff',
        marginHorizontal: 20,
        paddingHorizontal: 10,
        // borderRadius: 2,
        justifyContent: 'space-around',
    }
});