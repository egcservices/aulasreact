import React from 'react';
import {Toolbar, ToolbarContent} from 'react-native-paper';
import PropTypes from 'prop-types';
//command to install prop-types: npm install prop-types --save

const Header = ({title}) =>
    <Toolbar>
        <ToolbarContent title={title}/>
    </Toolbar>;

Header.propTypes = {
    title: PropTypes.string.isRequired
};
export default Header;