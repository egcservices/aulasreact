import React, {Component} from 'react'
import {AsyncStorage, StyleSheet} from 'react-native'
import {Modal, Provider as PaperProvider} from 'react-native-paper'
import {AddButton, AddTask, Header, TaskList} from './components/'

export default class App extends Component {
    state = {
        tasks: [],
        /*tasks: [
            {id: 0, name: 'Fazer exercício de RN', done: false},
            {id: 1, name: 'Estudar RN', done: false},
            {id: 2, name: 'Praticar RN', done: false},
        ],*/
        isShowModal: false,
    };

    async componentDidMount() {
        const tasks = await AsyncStorage.getItem('tasks');
        this.setState({tasks: JSON.parse(tasks) || []});
    };

    showModal = () => {
        this.setState({isShowModal: true});
    };

    renderModal = () => (
        <Modal
            visible={this.state.isShowModal}
            onDismiss={() => this.setState({isShowModal: false})}>
            <AddTask onTaskFilled={this.addTask}/>
        </Modal>
    );

    addTask = (name) => {
        const newTask = {
            id: new Date(),
            name: name,
            done: false
        };
        this.state.tasks.push(newTask);
        this.setState({tasks: [...this.state.tasks], isShowModal: false}, this.saveTasks);
    };

    saveTasks = () => {
        AsyncStorage.setItem('tasks', JSON.stringify(this.state.tasks));
    };

    changeTaskDone = (task) => {
        task.done = !task.done;
        this.setState({tasks: [...this.state.tasks]}, this.saveTasks);
    };

    deleteTask = (id) => {
        const tasks = this.state.tasks.filter((item) => item.id !== id);
        this.setState({tasks}, this.saveTasks);
    };

    render() {
        return (
            <PaperProvider>
                <Header title="Todo App"/>
                {this.renderModal()}
                <TaskList
                    style={styles.container}
                    tasks={this.state.tasks}
                    onChangeDone={this.changeTaskDone}
                    onDelete={this.deleteTask}/>
                <AddButton style={styles.fab} onPress={this.showModal}/>
            </PaperProvider>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#d9d2ff',
    },
    content: {
        marginTop: 10,
        padding: 5,
        // alignItems: 'center',
        // justifyContent: 'center',
    },
    fab: {
        position: 'absolute',
        bottom: 30,
        right: 30,
        backgroundColor: '#1b03dd'
    },
});
