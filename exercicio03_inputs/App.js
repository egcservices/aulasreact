import React, {Component} from 'react';
import {Button, Text, TextInput, View} from 'react-native';

export default class App extends Component {

    state = {
        peso: '',
        altura: '',
        imc: 'Seu IMC é: 0',
    };

    calculate = () => {
        const {peso, altura} = this.state;
        if (peso.length === 0 || altura.length === 0) {
            alert('Os campos são obrigatórios!');
            return;
        }
        if (!peso > 0 || !altura > 0) {
            alert('As medidas devem ser maiores que 0!');
            return;
        }
        const imc = 'Seu IMC é: ' + (peso / ((altura / 100) * (altura / 100)));
        this.setState({imc: imc});
    };

    render() {
        return (
            <View style={{flex: 1, backgroundColor: '#c8f'}}>
                <View style={{marginTop: 60, padding: 30}}>
                    <Text>Peso(kg)</Text>
                    <TextInput style={{
                        height: 40,
                        width: '100%'
                    }}
                               placeholder="Digite seu peso"
                               autoCapitalize="none"
                               keyboardType="numeric"
                               value={this.state.peso}
                               onChangeText={(text) => this.setState({peso: text})}
                    />

                    <Text>Altura(cm)</Text>
                    <TextInput style={{
                        height: 40,
                        width: '100%'
                    }}
                               placeholder="Digite sua altura"
                               autoCapitalize="none"
                               keyboardType="numeric"
                               value={this.state.altura}
                               onChangeText={(text) => this.setState({altura: text})}
                    />
                    <Button style={{marginTop: 20}} title="Calcular" onPress={this.calculate}/>
                    <Text style={{marginTop: 20}}>{this.state.imc}</Text>
                </View>
            </View>
        );
    }
}