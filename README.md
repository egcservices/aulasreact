# ReactNative - Aulas #

## Requisitos ##
  - Ter o NodeJS e NPM instalado.
 
### Como baixar o projeto ###
  - git clone https://egcservices@bitbucket.org/egcservices/aulasreact.git

### Scripts disponíveis ###
  - npm start
  - npm test
  - npm run ios
  - npm run android
  - npm run eject
  
##### Qualquer dúvida consultar o README.md dos projetos #####

### FIM ###