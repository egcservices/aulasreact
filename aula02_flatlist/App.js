import React from 'react';
import {FlatList, Platform, StyleSheet, Text, TouchableNativeFeedback, TouchableOpacity, View} from 'react-native';

const Touchable = Platform.OS === 'ios' ? TouchableNativeFeedback : TouchableOpacity;

export default class App extends React.Component {

    cities = [
        {name: 'Recife', state: 'PE'},
        {name: 'Olinda', state: 'PE'},
        {name: 'Jaboatão', state: 'PE'},
        {name: 'Vitória Sº Antão', state: 'PE'},
        {name: 'São Paulo', state: 'SP'},
        {name: 'Rio de Janeiro', state: 'SE'},
        {name: 'Aracajú', state: 'SE'},
        {name: 'Manaus', state: 'AM'},
        {name: 'Curitiba', state: 'PR'},
        {name: 'Belém', state: 'PA'},
    ];

    render() {

        return (
            <View style={styles.container}>
                <FlatList
                    style={styles.content}
                    data={this.cities}
                    keyExtractor={(city) => city.name}
                    renderItem={({item, index}) =>
                        <Touchable style={styles.row}
                                   onPress={() => alert(`Você clicou na cidade ${item.name} do estado de ${item.state}`)}>
                            <Text>{item.name} - {item.state}</Text>
                        </Touchable>
                    }
                />
            </View>
        );

        // return (
        //     <View style={styles.container}>
        //         <ScrollView style={styles.content}>
        //             {this.cities.map((city) =>
        //                 <View key={city.name} style={styles.row}>
        //                     <Text style={styles.item}>{city.name} - {city.state}</Text>
        //                 </View>
        //             )}
        //         </ScrollView>
        //     </View>
        // );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        paddingTop: 50,
        // alignItems: 'center',
        // justifyContent: 'center',
    },
    content: {
        marginTop: 10,
        padding: 5,
    },
    row: {
        backgroundColor: '#f6ff7e',
        height: 40,
        marginVertical: 2,
        justifyContent: 'center',
        paddingLeft: 10,
    },
    item: {}
});
