import React, {Component} from 'react';
import {FlatList, RefreshControl, View} from 'react-native';
import {getCoins} from '../services';
import ListItem from '../components/ListItem';

export default class HomeScreen extends Component {
    static navigationOptions = {
        title: 'Crypto Coin Market'
    };

    state = {
        coins: [],
        isLoading: false,
    };

    async componentDidMount() {
        this.loadData();
    }

    loadData = async () => {
        const response = await getCoins();
        this.setState({coins: response.data});
    };

    showCoinDetails = (coin) => {
        this.props.navigation.navigate('Coin', {coin});
    };

    render() {
        return (
            <FlatList
                data={this.state.coins}
                renderItem={({item}) =>
                    <ListItem
                        item={item}
                        onItemPress={() => this.showCoinDetails(item)}
                    />}
                keyExtractor={(item) => item.id.toString()}
                ItemSeparatorComponent={() => <View style={{height: 4, backgroundColor: '#fff'}}/>}
                refreshing={this.state.isLoading}
                refreshControl={
                    <RefreshControl
                        refreshing={this.state.isLoading}
                        onRefresh={this.loadData}/>
                }
            />
        );
    }
}