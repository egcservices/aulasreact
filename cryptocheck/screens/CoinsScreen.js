import React, {Component} from 'react';
import {Dimensions, ScrollView, StyleSheet, Text, View} from 'react-native';
import {LineChart} from 'react-native-chart-kit';
import numbro from 'numbro';
import moment from 'moment';
import {getLast7Days} from '../services';

export default class CoinsScreen extends Component {
    static navigationOptions = ({navigation}) => ({
        title: navigation.getParam('coin').name,
        headerTintColor: '#000',
    });


    state = {
        chartData: {
            labels: [],
            values: [],
        }
    };

    async componentDidMount() {
        const coin = this.props.navigation.getParam('coin');
        const chartData = await getLast7Days(coin.symbol);
        this.setState({chartData});
    };

    render() {
        const coin = this.props.navigation.getParam('coin');
        const date = moment.unix(coin.last_updated);

        return (
            <ScrollView>
                <View style={styles.contentHeader}>
                    <Text style={{fontSize: 24}}>
                        ${numbro(coin.quotes.USD.price).format({
                        thousandSeparated: true,
                        mantissa: 2
                    })}</Text>
                    <Text style={{color: 'gray'}}>
                        {date.format("dddd, MMMM Do YYYY, h:mm:ss a")}
                    </Text>
                </View>

                <LineChart
                    data={{
                        labels: this.state.chartData.labels,
                        datasets: [{
                            data: this.state.chartData.values
                        }]
                    }}
                    width={Dimensions.get('window').width - 10} // from react-native
                    height={220}
                    chartConfig={{
                        backgroundGradientFrom: '#fff',
                        backgroundGradientTo: '#fff',
                        color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
                        style: {
                            borderRadius: 0,
                        }
                    }}
                    style={{
                        marginVertical: 10,
                        marginHorizontal: 5,
                    }}
                />
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    contentHeader: {
        width: '100%',
        alignItems: 'center',
        marginTop: 20,
    }
});
