import React from 'react';
import {createStackNavigator} from 'react-navigation';
import HomeScreen from './screens/HomeScreen';
import CoinsScreen from './screens/CoinsScreen';

const App = createStackNavigator({
    Home: {
        screen: HomeScreen
    },
    Coin: {
        screen: CoinsScreen
    }
});

export default App;
