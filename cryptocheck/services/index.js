import moment from 'moment';

export const getCoins = async () => {
    const response = await fetch('https://api.coinmarketcap.com/v2/ticker/?limit=3&structure=array');
    return await response.json();
};

export const getLast7Days = async (symbol) => {
    const result = {
        labels: [],
        values: [],
    };

    for (let i = 6; i >= 0; i--) {
        const date = moment().subtract(i, 'days');
        const response = await fetch(`https://min-api.cryptocompare.com/data/pricehistorical?fsym=${symbol}&tsyms=USD&ts=${date.unix()}`);
        const data = await response.json();
        result.labels.push(date.format('D. MMM'));
        result.values.push(data[symbol].USD);
    }

    return result;
};