import React, {Component} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import numbro from 'numbro';

export default class ListItem extends Component {

    renderChange = (value) => {
        return <Text style={{
            color: value.toFixed(1) >= 0 ? 'green' : 'red',
            marginHorizontal: 10,
            width: 60,
            textAlign: 'right',
        }}>{value.toFixed(1) >= 0 ? '+' : ''}{value}%</Text>
    };

    render() {
        const {item, onItemPress} = this.props;
        return (
            <TouchableOpacity style={styles.row} onPress={onItemPress}>
                <Text style={styles.rankText}>{item.rank}</Text>
                <Image
                    style={styles.coinImage}
                    source={{uri: `https://s2.coinmarketcap.com/static/img/coins/32x32/${item.id}.png`}}/>
                <View>
                    <Text>{item.name}</Text>
                    <Text>{item.symbol}</Text>
                </View>
                <Text style={styles.priceText}>
                    $ {numbro(item.quotes.USD.price).format({
                    thousandSeparated: true,
                    mantissa: 2
                })}
                </Text>
                {this.renderChange(item.quotes.USD.percent_change_24h)}
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    row: {
        height: 60,
        flexDirection: 'row',
        alignItems: 'center',

    },
    rankText: {
        width: 30,
        textAlign: 'right'
    },
    coinImage: {
        width: 32,
        height: 32,
        marginHorizontal: 10,
    },
    priceText: {
        flex: 1,
        textAlign: 'right',
        paddingRight: 20,
    },
});