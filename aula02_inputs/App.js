import React, {Component} from 'react';
import {Button, TextInput, View} from 'react-native';

export default class App extends Component {

    state = {
        email: '',
        password: ''
    };

    login = () => {
        if (this.state.email.length === 0 || this.state.password.length === 0) {
            alert('Os campos são obrigatórios.');
            return;
        }
        alert(`Usuário ${this.state.email} autenticado`);
    };

    render() {
        return (
            <View style={{flex: 1, marginTop: 60, backgroundColor: '#c4f'}}>
                <TextInput style={{
                    height: 40,
                    width: '100%'
                }}
                           placeholder="Digite um e-mail"
                           autoCapitalize="none"
                           keyboardType="email-address"
                           value={this.state.email}
                           onChangeText={(text) => this.setState({email: text})}
                />
                <TextInput style={{
                    height: 40,
                    width: '100%'
                }}
                           secureTextEntry
                           placeholder="Digite uma senha"
                           autoCapitalize="none"
                           value={this.state.password}
                           onChangeText={(text) => this.setState({password: text})}
                />
                <Button title="Acessar" onPress={this.login}/>
            </View>
        );
    }
}