import React from 'react';
import {Text} from 'react-native';

const HelloFunc = ({texto, style}) => {
    return <Text style={{
        color: style.cor,
        fontSize: style.tam
    }}>{texto}</Text>
};

export default HelloFunc;