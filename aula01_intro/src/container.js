import React, {Component} from 'react';
import {View} from 'react-native';

export default class Container extends Component {

    render() {
        return (
            <View style={{
                width: 200,
                height: 200,
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: "#cfcfcf"
            }}>{this.props.children}</View>
        );
    }
}


