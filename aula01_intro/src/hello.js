import React, {Component} from 'react';
import {Text} from 'react-native';

export default class Hello extends Component {
    render() {

        //passing obj with consts
        const {texto, style} = this.props;
        return (
            <Text style={{color: style.cor, fontSize: style.tam}}>
                {texto}
            </Text>
        );


        //passing obj with consts
        /*const {cor, texto, tam} = this.props;
        return (
            <Text style={{color: cor, fontSize: tam}}>
                {texto}
            </Text>
        );*/

        //passing props in style direct
        /*return (
            <Text style={{color: this.props.cor, fontSize: this.props.tam}}>
                {this.props.texto}
            </Text>
        );*/
    }
}