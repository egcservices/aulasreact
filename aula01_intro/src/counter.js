import React, {Component} from 'react';
import {Button, Text, View} from 'react-native';

export default class Counter extends Component {

    state = {
        counter: 0
    };

    // constructor() {
    //     super();
    //     this.inc = this.inc.bind(this);
    //     this.dec = this.dec.bind(this);
    // }

    inc = () => {
        this.setState({counter: this.state.counter + 1})
    };

    dec = () => {
        this.setState({counter: this.state.counter - 1})
    };

    render() {
        return (
            <View style={{marginTop: 20}}>
                <Text style={{fontSize: 20}}>Contador: {this.state.counter}</Text>
                <Button title="Add" onPress={() => {
                    this.inc()
                }}/>
                <Button title="Rem" onPress={() => {
                    this.dec()
                }}/>
            </View>
        );
    }
}