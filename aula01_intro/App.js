import React, {Component} from 'react';
import {Button, Platform, StyleSheet, View} from 'react-native';
import Hello from './src/hello'
import HelloFunc from './src/hellofunc'
import Container from "./src/container";
import Counter from "./src/counter";

const Box = () => {
    return (
        <View style={styles.container}>
            <View style={styles.internal}/>
        </View>
    )
};

export default class App extends Component {

    render() {

        //using box
        return <Box/>;

        //using func prop
        return (
            <View style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: '#c4f',
            }}>
                <Hello
                    texto="Exemplo 01 aula React Native"
                    style={{tam: 30, cor: "blue"}}/>
                <Hello
                    texto="Gostaram?!"
                    style={{tam: 20, cor: 'red'}}/>

                <HelloFunc
                    texto='Exemplo de HelloFunc'
                    style={{tam: 22, cor: 'green'}}/>

                {/*using container*/}
                <Container>
                    <View style={{
                        height: 150,
                        width: 150,
                        justifyContent: 'center',
                        alignItems: 'center',
                        backgroundColor: 'red'
                    }}>
                        <Button title="Aí sim viuu!" onPress={() => {
                        }}/>
                    </View>
                </Container>

                <Button title="Exibir plataforma" onPress={() => {
                    alert(Platform.OS);
                }}/>

                <Counter/>

            </View>
        );

        //using style how object
        /*return (
            <View style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: '#c4f',
            }}>
                <Hello
                    texto="Exemplo 01 aula React Native"
                    style={{tam: 30, cor: "blue"}}/>
                <Hello
                    texto="Gostaram?!"
                    style={{tam: 20, cor: 'red'}}
                />
            </View>
        );*/

        //passing props in style direct
        /*return (
            <View style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: '#c4f',
            }}>
                <Hello tam={22} texto="Exemplo 01 aula React Native" cor="blue"/>
                <Hello tam={17} texto={'Gostaram?!'} cor="red"/>

                <HelloFunc texto="Exemplo de HelloFunc"/>
            </View>
        );*/
    }
}

const styles = StyleSheet.create({
    container: {
        // flex: 1,
        backgroundColor: '#c4f',
        //alignItems: 'center',
        //justifyContent: 'center',
        width: 500,
        height: 500,
    },
    internal: {
        // flex: 1,
        width: 300,
        height: 300,
        borderRadius: 20,
        borderWidth: 2,
        borderColor: 'red',
        backgroundColor: 'skyblue'
    },
    field: {
        fontSize: 70,
        color: "#fff",
    },
});