import React, {Component} from 'react';
import {ScrollView, View} from 'react-native';

export default class App extends Component {
    render() {
        return (
            <ScrollView style={{flex: 1}}>
                <ScrollView style={{height: 200}} horizonal>
                    <View style={{backgroundColor: 'red', height: 200, width: 200}}/>
                    <View style={{backgroundColor: 'blue', height: 200, width: 200}}/>
                    <View style={{backgroundColor: 'yellow', height: 200, width: 200}}/>
                </ScrollView>
                <View style={{backgroundColor: 'red', height: 200, width: 200}}/>
                <View style={{backgroundColor: 'blue', height: 700, width: 200}}/>
                <View style={{backgroundColor: 'yellow', height: 200, width: 200}}/>
            </ScrollView>
        );
    }
}