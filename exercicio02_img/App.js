import React, {Component} from 'react';
import {Image, StyleSheet, Text, View,} from 'react-native';
import Model2 from "./src/model2";

export default class App extends Component {
    render() {

        return <Model2/>

        return (
            <View style={styles.view}>
                <Image style={styles.image}
                       source={source2}/>
                <Text style={styles.text}>Um texto qualquer</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    view: {
        marginTop: 30,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#c4f',
        // backgroundColor: '#fff',
    },
    image: {
        width: 200,
        height: 200,
        borderRadius: 40,
    },
    text: {
        marginTop: 25,
        fontSize: 25,
        fontWeight: 'bold',
    }
});

const source = {uri: 'https://i.stack.imgur.com/Mmww2.png'};
const source2 = {uri: 'https://dzwonsemrish7.cloudfront.net/items/1o2V1d0V063w3B2a0I26/tyrion.jpg'};
