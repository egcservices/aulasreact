import React, {Component} from 'react';
import {Image, StyleSheet, Text, View,} from 'react-native';
//import img from 'tyrion.jpg';


export default class Model2 extends Component {
    render() {
        return (
            <View style={styles.view}>
                <Image style={styles.image}
                       source={source}/>
                <View style={styles.viewBody}>
                    <Text style={styles.text_title}>{myText.title}</Text>
                    <Text style={styles.text_body}>{myText.body}</Text>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    view: {
        marginTop: 25,
        flex: 1,
        flexDirection: 'row',
        backgroundColor: '#c4f',
    },
    viewBody: {
        flex: 1,
        paddingHorizontal: 5,
    },
    image: {
        width: 150,
        height: 150,
    },
    text_title: {
        fontSize: 20,
        fontWeight: 'bold',
    },
    text_body: {
        fontSize: 12,
    },
});

const source = {uri: 'https://dzwonsemrish7.cloudfront.net/items/1o2V1d0V063w3B2a0I26/tyrion.jpg'};
const myText = {
    title: "Anão Tryan",
    body: "Personagem de uma série chamada Games of Thrones."
};
